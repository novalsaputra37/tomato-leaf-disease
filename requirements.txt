Django==3.2.15
djangorestframework==3.14.0
Pillow==9.2.0
numpy==1.23.3
tensorflow==2.10.0

# jupyter
# pandas
# matplotlib
