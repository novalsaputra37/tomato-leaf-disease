$(document).ready(function () {
    // Init
    $('.image-section').hide();
    $('.loader').hide();
    $('.input-image').show();
    $('#result').hide();

    // Upload Preview
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
                $('.input-image').hide();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function () {
        $('.image-section').show();
        $('#btn-predict').show();
        $('#result').text('');
        $('#result').hide();
        readURL(this);
    });

    // Predict
    $('#btn-predict').click(function () {
        var form_data = new FormData($('#upload-file')[0]);

        // Show loading animation
        $(this).hide();
        $('.upload-hapus').hide();
        $('.upload-selesai').hide();
        $('.loader').show();

        // Make prediction by calling api /predict
        $.ajax({
            type: 'POST',
            url: '/predict/',
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            dataType: "json",
            success: function (data) {
                // Get and display the result
                $('.loader').hide();
                $('#result').fadeIn(600);
                if (data.data.accuracy != null) {
                    $('#result').text(`${data.data.name_virus} : ${data.data.accuracy}`);
                }
                else{
                    $('#result').text(`${data.data.name_virus}`);
                }
                $('.row').show();
                $('.upload-hapus').show();
                $('.upload-selesai').show();
            },
        });
    });
});