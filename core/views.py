from django.shortcuts import render
from .healper import prediction
from .models import LeafModel
from django.core.files.storage import FileSystemStorage

from rest_framework.decorators import api_view
from rest_framework.response import Response

def beranda(request):
    get_data_context = LeafModel.objects.get(id=1)
    context = {
        "title" : "beranda",
        "context": get_data_context,
    }

    return render(request, 'beranda.html', context)

def informasi(request):
    """ get information of leaf from model """
    leaf_list = LeafModel.objects.all().order_by('id').exclude(id=1)
    
    """ data output """
    context = {
        "title" : "informasi",
        "leafs" : leaf_list,
    }

    return render(request, 'informasi.html', context)

def detail_informasi(request, pk=None):
    if pk:
        leaf_detail = LeafModel.objects.get(pk=pk)
        return render(request, 'informasi-detail.html', {"title": "informasi", "leaf": leaf_detail})
    else:
        return render(request, 'informasi.html', {"title": "informasi"})

def diagnosa(request):
    return render(request, 'diagnosa.html', {"title": "diagnosa"})


@api_view(['GET', 'POST'])
def predict(request):
    if request.method == 'POST':
        """ get image from website"""
        image = request.data['file']
        if image:
            """ save image"""
            fss = FileSystemStorage(location='media/spam')
            file = fss.save(image.name, image)
            file_url = fss.url(file)

            """ process prediction of tomato leaf disease """
            virus, accuracy = prediction(image)

            """ output data to website """
            context = {
                "title": "Diagnosa",
                "image": file_url,
                "name_virus": virus,
                # "accuracy": accuracy,
            }
            fss.delete(image.name)
            return Response({"data": context})
        else:
            return Response({"massage": "image not found"})
    else:
        return Response({"massage": "method is not allowed "})
