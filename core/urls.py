from django.contrib import admin
from django.urls import path
from .views import beranda, informasi, diagnosa, detail_informasi, predict
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', beranda, name="beranda"),
    path('informasi/', informasi, name="informasi"),
    path('informasi/<pk>', detail_informasi, name="informasi-detail"),
    path('diagnosa/', diagnosa, name="diagnosa"),
    path('predict/', predict, name='predict'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
