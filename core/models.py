from django.db import models
from django.utils.text import slugify

class LeafModel(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    img = models.ImageField(upload_to='leaf')
    slug = models.SlugField(blank=True, editable = False)

    def seve(self):
        self.slug = slugify(self.name)
        super(LeafModel, self).save()

    def __str__(self):
        return self.name