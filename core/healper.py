import os
import numpy as np
from PIL import Image
from tensorflow.keras.models import load_model
from django.conf import settings

def preprocess(img,input_size):
    """ resize image """
    nimg = img.convert('RGB').resize(input_size, resample= 0)
    img_arr = (np.array(nimg))/255
    return img_arr


def prediction(input_image):

    """ class from model """
    labels = [
        "Bacterial spot",
        "Early blight",
        "Late blight",
        "Leaf Mold",
        "Septoria leaf spot",
        "Spider mites Two-spotted spider mite",
        "Target Spot",
        "Tomato Yellow Leaf Curl Virus",
        "Tomato mosaic virus",
        "Healthy"
    ]
    
    """ resize and expand image """
    image = preprocess(Image.open(input_image),(224,224))
    expand_image = np.expand_dims(image, axis=0)
    
    """ load model"""
    model = load_model(os.path.join(settings.BASE_DIR, settings.BASE_MODEL_DIR), compile=False)
    
    """ predict model """
    preds = model.predict(expand_image)
    index_preds = np.argmax(preds)
    accuracy = f"{np.max(preds)*100}%"

    """" result """
    return labels[index_preds], accuracy